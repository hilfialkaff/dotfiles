autocmd! BufNewFile,BufRead *.pde setlocal ft=arduino
autocmd! BufNewFile,BufRead Makefrag setlocal ft=make
autocmd! BufNewFile,BufRead *.vimp* setlocal ft=vim
autocmd! BufNewFile,BufRead *.ned setlocal ft=ned
