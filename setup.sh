sudo apt-get install -y tmux zsh
cp .tmux.conf ~
cp .zshrc ~
cp .vim* ~ -r
cp .gitconfig ~
cp .gitignore_global ~
